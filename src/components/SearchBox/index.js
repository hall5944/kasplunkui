import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';

class SearchBox extends React.PureComponent {
    render () {
        const {query, onChange, onSearch} = this.props;
        return (
            <div className="container row py-3">
                <input
                 type="text"
                 value={query}
                 onChange={(e) => onChange(e.target.value)}
                 className="col-10"
                />
                <button className="btn btn-primary col-2" onClick={onSearch}>
                    <FontAwesomeIcon icon={faSearch} />
                </button>
            </div>
        )
    }
}

SearchBox.propTypes = {
    query: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onSearch: PropTypes.func.isRequired,
}

export default SearchBox;