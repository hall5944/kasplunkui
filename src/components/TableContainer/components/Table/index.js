import React from 'react';
import './styles.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faChartBar} from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';

class Table extends React.PureComponent {
    constructor(props) {
        super(props);
    }s

    getTableHeader (data) {
        const headers = data.map(item => <th scope='col'>{item}</th>);
        return (
            <thead>
                <tr>
                { headers }
                </tr>
            </thead>
        );
    }

    getTableBody (data) {
        const rows = [];
        for(const row of data) {
            const columns = row.map(item => <td> {item} </td>);
            rows.push(<tr>{columns}</tr>);
        }

        return (
                <tbody>
                    {rows}
                </tbody>
        );
    }

    getTableComponent (data) {
        const headerData = data[0];
        const bodyData = data.slice(1,data.length);
        const header = this.getTableHeader(headerData);
        const body = this.getTableBody(bodyData);

        return (
            <div className="overflow-auto w-100 table-container">
                <table className="table table-striped table-dark w-100">
                    {header}
                    {body}
                </table>
            </div>
        );
    }


    render() {
        const { data } = this.props;
        const table = this.getTableComponent(data);
        return( table );
    }
}

Table.propTypes = {
    data: PropTypes.array.isRequired
}

export default Table;
