import React from 'react';
//import Table from './components/Table';
import './styles.css';
import Table from './components/Table';
import PropTypes from 'prop-types';

class TableContainer extends React.PureComponent {

    render() {
        const { data } = this.props;
        return(
            <div className="row">
            <Table data={data}/>
            </div>
        );
    }
}

export default TableContainer;
