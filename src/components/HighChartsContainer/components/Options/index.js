import React from 'react';
import './styles.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faChartBar, faChartPie, faChartLine, faProjectDiagram} from '@fortawesome/free-solid-svg-icons';


class Options extends React.PureComponent {
    constructor(props) {
        super(props);
        this.optionsTemplate = "{ \"color\": \"red\" }";
    }
    render() {
        return(
            <div>
            <h5>Developer Editor</h5>
            <div className="row my-1">
                <button class="btn mr-1 rounded-circle btn-primary"><FontAwesomeIcon icon={faChartBar}/></button>
                <button class="btn mr-1 rounded-circle btn-primary"><FontAwesomeIcon icon={faChartPie}/></button>
                <button class="btn mr-1 rounded-circle btn-primary"><FontAwesomeIcon icon={faChartLine}/></button>
                <button class="btn mr-1 rounded-circle btn-primary"><FontAwesomeIcon icon={faProjectDiagram}/></button>
            </div>
            <div className="overflow-auto options-box border rounded" contentEditable='true'>{this.optionsTemplate}</div>
            <div className="row my-1">
                <button class="btn btn-primary col-6">Preview</button>
                <button class="btn btn-primary col-6">KaSplunk</button>
            </div>
            </div>
        );
    }
}

export default Options;
