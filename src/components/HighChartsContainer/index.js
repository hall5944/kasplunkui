import React from 'react';
import Options from './components/Options';
import HighCharts from './components/HighCharts';
import './styles.css';

class HighChartsContainer extends React.PureComponent {
    render() {
        return(
            <div className="row placeholder-hc-container">
                <div className="col-3 m-0">
                    <Options/>
                </div>
                <div className="col-9 m-0 p-0">
                    <HighCharts/>
                </div>
            </div>
        );
    }
}

export default HighChartsContainer