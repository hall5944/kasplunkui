import React from 'react';
import './App.css';
import SearchBox from './components/SearchBox';
import Header from './components/Header';
import HighChartsContainer from './components/HighChartsContainer';
import TableContainer from './components/TableContainer';
import * as HTTPService from './services/http.service';

class App extends React.Component {

  state = {
    query: '',
    tableData:  [
                    ["month", "mLs","temperature"],
                    ["January",20,25],
                    ["February",11,2],
                    ["March",34,11],
                    ["March",34,11],
                    ["March",34,11],
                    ["March",34,11],
                    ["March",34,11],
                    ["March",34,11],
                    ["March",34,11],
                    ["March",34,11],
                    ["March",34,11],
                    ["March",34,11],
                    ["March",34,11]
                ]
  }

  onSearch = () => {
    console.log('search pressed');
    const { query } = this.state;
    HTTPService.getSearchResults(query).then(res => {
        console.log(res);
        this.setState({tableData: res.data});
    });

  }

  onChange = (value) => {
    this.setState({query: value});
    console.log('value changed', value);
  }

  render() {
    const {query, tableData} = this.state;
    return (
      <>
        <Header/>
        <div className="container">
          <SearchBox
          query={query}
          onSearch={() => this.onSearch()}
          onChange={(val) => this.onChange(val)}
          />
          <HighChartsContainer />
          <TableContainer data={ tableData }/>
        </div>
      </>
    );
  }
}

export default App;
