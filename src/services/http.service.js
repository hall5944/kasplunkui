const http = require('axios');

export async function getSearchResults(query) {
    const result = await http.post('http://localhost:8080/search', { query });
    return result;
}
